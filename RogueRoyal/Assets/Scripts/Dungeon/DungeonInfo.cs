﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon
{
    /// <summary>
    /// ランダムダンジョン作成時に使う変数まとめ
    /// </summary>
    public class DungeonInfo
    {
        /// <summary>
        /// マップ配列の中身の定数
        /// </summary>
        public enum ID
        {
            WALL = 10,
            ROAD = 20,
            ITEM = 30,
            ROOM = 50
        }

        /// <summary>
        /// マップの横幅
        /// </summary>
        [HideInInspector] public const int width = 40;
        /// <summary>
        /// マップの縦幅
        /// </summary>
        [HideInInspector] public const int height = 40;
        /// <summary>
        /// マップの二次元配列
        /// </summary>
        [HideInInspector] public static int[,] dungeonMap;
        /// <summary>
        /// 最小の部屋数
        /// </summary>
        [HideInInspector] public const int minRooms = 10;
        /// <summary>
        /// 最大の部屋数
        /// </summary>
        [HideInInspector] public const int maxRooms = 20;
        /// <summary>
        /// 最も小さい部屋の大きさ
        /// </summary>
        [HideInInspector] public const int roomMinSize = 4;
    }

    /// <summary>
    /// マップを分割する際の変数まとめ
    /// </summary>
    public class SplitRoomInfo
    {
        public int roomTop = 0;
        public int roomLeft = 0;
        public int roomBottom = 0;
        public int roomRight = 0;
        public int roomArea = 0;
        public int nextRoom = 0;
        public bool howToSplit = false;
        public int NextRoomPos = 0;
    }
}