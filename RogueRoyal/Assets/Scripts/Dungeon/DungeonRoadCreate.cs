﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Dungeon.DungeonInfo;   // 変数はDungeonInfoから

namespace Dungeon
{
    public class DungeonRoadCreate : MonoBehaviour
    {
        /// <summary>
        /// 道を作成
        /// </summary>
        public void CreateRoad()
        {
            int NOWpos = 0;
            int NOWdis = 0;
            int NEXTpos = 0;
            int NEXTdis = 0;
            for (int i = 1; i < DungeonRoomCreate.roomNum; i++)
            {
                if (DungeonRoomCreate.mapSplit[i].howToSplit)
                {
                    // 通路の開始地点、及び終了地点を求める
                    NOWpos = DungeonRoomCreate.mapSplit[i].roomBottom - DungeonRoomCreate.mapSplit[i].roomTop;
                    NOWpos = Random.Range(1, NOWpos) + DungeonRoomCreate.mapSplit[i].roomTop;
                    NEXTpos = DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomBottom - DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomTop;
                    NEXTpos = Random.Range(1, NEXTpos) + DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomTop;

                    // 通路を引く線の長さを（開始、終了地点共に）求める
                    NOWdis = DungeonRoomCreate.mapSplit[i].roomLeft - DungeonRoomCreate.mapSplit[i].NextRoomPos + 1;
                    NEXTdis = DungeonRoomCreate.mapSplit[i].NextRoomPos - DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomRight + 1;


                    // ←ライン作成
                    for (int j = 0; j < NOWdis; j++)
                    {
                        dungeonMap[-j + DungeonRoomCreate.mapSplit[i].roomLeft, NOWpos] = (int)ID.ROAD;
                    }

                    // →ライン作成
                    for (int j = 0; j < NEXTdis; j++)
                    {
                        dungeonMap[j + DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomRight, NEXTpos] = (int)ID.ROAD;
                    }

                    // 縦ライン作成
                    for (int j = 0; ; j++)
                    {
                        // NOWとNEXT、どちらの方が高さが大きいか調べ、縦ラインを作成する
                        if (NOWpos >= NEXTpos)
                        {
                            if ((NEXTpos + j) < NOWpos)
                            {
                                dungeonMap[DungeonRoomCreate.mapSplit[i].NextRoomPos, NEXTpos + j] = (int)ID.ROAD;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if ((NOWpos + j) < NEXTpos)
                            {
                                dungeonMap[DungeonRoomCreate.mapSplit[i].NextRoomPos, NOWpos + j] = (int)ID.ROAD;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                }
                else
                {
                    // 通路の開始地点、及び終了地点を求める
                    NOWpos = DungeonRoomCreate.mapSplit[i].roomRight - DungeonRoomCreate.mapSplit[i].roomLeft;
                    NOWpos = Random.Range(1, NOWpos) + DungeonRoomCreate.mapSplit[i].roomLeft;
                    NEXTpos = DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomRight - DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomLeft;
                    NEXTpos = Random.Range(1, NEXTpos) + DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomLeft;

                    // 通路を引く線の長さを（開始、終了地点共に）求める
                    NOWdis = DungeonRoomCreate.mapSplit[i].roomTop - DungeonRoomCreate.mapSplit[i].NextRoomPos + 1;
                    NEXTdis = DungeonRoomCreate.mapSplit[i].NextRoomPos - DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomBottom + 1;


                    // ↑ライン作成
                    for (int j = 0; j < NOWdis; j++)
                    {
                        dungeonMap[NOWpos, -j + DungeonRoomCreate.mapSplit[i].roomTop] = (int)ID.ROAD;
                    }

                    // ↓ライン作成
                    for (int j = 0; j < NEXTdis; j++)
                    {
                        dungeonMap[NEXTpos, j + DungeonRoomCreate.mapSplit[DungeonRoomCreate.mapSplit[i].nextRoom].roomBottom] = (int)ID.ROAD;
                    }

                    // 横ライン作成
                    for (int j = 0; ; j++)
                    {
                        // NOWとNEXT、どちらの方が高さが大きいか調べ、横ラインを作成する
                        if (NOWpos >= NEXTpos)
                        {
                            if ((NEXTpos + j) < NOWpos)
                            {
                                dungeonMap[NEXTpos + j, DungeonRoomCreate.mapSplit[i].NextRoomPos] = (int)ID.ROAD;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if ((NOWpos + j) < NEXTpos)
                            {
                                dungeonMap[NOWpos + j, DungeonRoomCreate.mapSplit[i].NextRoomPos] = (int)ID.ROAD;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 障害物を作成
        /// </summary>
        public void CreateObj()
        {
            int x;
            int y;
            int objNum = 0;
            int objID = 0;

            while (objNum <= 10)
            {
                x = Random.Range(0, width);
                y = Random.Range(0, height);

                if (dungeonMap[x, y] >= (int)ID.ROOM)
                {
                    objID = Random.Range(0, 4);

                    

                    objNum++;
                }
            }
        }
    }
}