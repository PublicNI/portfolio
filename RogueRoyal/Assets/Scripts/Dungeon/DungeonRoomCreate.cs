﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Dungeon.DungeonInfo;

namespace Dungeon
{
    public class DungeonRoomCreate : MonoBehaviour
    {
        public static int roomNum;
        public static List<SplitRoomInfo> mapSplit = new List<SplitRoomInfo>();

        /// <summary>
        /// 壁で全面埋めたマップデータを作成
        /// </summary>
        public void InitialMapData()
        {
            dungeonMap = new int[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    dungeonMap[i, j] = (int)ID.WALL;
                }
            }
        }

        /// <summary>
        /// マップを分割する
        /// </summary>
        public void MapSplit()
        {
            int dviPos;
            roomNum = Random.Range(minRooms, maxRooms);
            mapSplit.Add(new SplitRoomInfo());
            mapSplit[0].roomTop = 0;
            mapSplit[0].roomLeft = 0;
            mapSplit[0].roomBottom = height;
            mapSplit[0].roomRight = width;
            mapSplit[0].roomArea = mapSplit[0].roomBottom + mapSplit[0].roomRight;

            for (int i = 1; i < roomNum; i++)
            {
                mapSplit.Add(new SplitRoomInfo());
                int Target = 0;
                int AreaMax = 0;
                // 最大の面積を持つ区画を指定する
                for (int j = 0; j < i; j++)
                {
                    if (mapSplit[j].roomArea >= AreaMax)
                    {
                        AreaMax = mapSplit[j].roomArea;
                        Target = j;
                    }
                }

                // 分割点を求める
                if ((mapSplit[Target].roomBottom - mapSplit[Target].roomTop) > 12 && (mapSplit[Target].roomRight - mapSplit[Target].roomLeft) > 12)
                {
                    mapSplit[i].nextRoom = Target;
                    dviPos = Random.Range(0, mapSplit[Target].roomArea);

                    if (dviPos > (mapSplit[Target].roomBottom - mapSplit[Target].roomTop))
                    {
                        mapSplit[i].roomLeft = mapSplit[Target].roomLeft + Random.Range(6, mapSplit[Target].roomRight - mapSplit[Target].roomLeft - 6);
                        mapSplit[i].roomRight = mapSplit[Target].roomRight;
                        mapSplit[Target].roomRight = mapSplit[i].roomLeft - 1;
                        mapSplit[i].roomTop = mapSplit[Target].roomTop;
                        mapSplit[i].roomBottom = mapSplit[Target].roomBottom;
                        mapSplit[i].howToSplit = true;
                        mapSplit[i].NextRoomPos = mapSplit[i].roomLeft;
                    }
                    else
                    {
                        mapSplit[i].roomTop = mapSplit[Target].roomTop + Random.Range(6, mapSplit[Target].roomBottom - mapSplit[Target].roomTop - 6);
                        mapSplit[i].roomBottom = mapSplit[Target].roomBottom;
                        mapSplit[Target].roomBottom = mapSplit[i].roomTop - 1;
                        mapSplit[i].roomLeft = mapSplit[Target].roomLeft;
                        mapSplit[i].roomRight = mapSplit[Target].roomRight;
                        mapSplit[i].howToSplit = false;
                        mapSplit[i].NextRoomPos = mapSplit[i].roomTop;

                    }

                    mapSplit[i].roomArea = mapSplit[i].roomBottom - mapSplit[i].roomTop + mapSplit[i].roomRight - mapSplit[i].roomLeft;
                    mapSplit[Target].roomArea = mapSplit[Target].roomBottom - mapSplit[Target].roomTop + mapSplit[Target].roomRight - mapSplit[Target].roomLeft;
                }
                else
                {
                    roomNum = i;
                    break;
                }
            }
        }

        /// <summary>
        /// 部屋のIDをdungeonMapに割り振って部屋を作る
        /// </summary>
        public void CreateRoom()
        {
            int ratioX;
            int ratioY;
            int moveX;
            int moveY;
            for (int i = 0; i < roomNum; i++)
            {
                if (mapSplit[i] != null)
                {
                    ratioY = mapSplit[i].roomBottom - mapSplit[i].roomTop;
                    ratioY = Mathf.FloorToInt(Random.Range(0.5f, 0.8f) * ratioY);
                    ratioX = mapSplit[i].roomRight - mapSplit[i].roomLeft;
                    ratioX = Mathf.FloorToInt(Random.Range(0.5f, 0.8f) * ratioX);
                    moveY = Mathf.FloorToInt((mapSplit[i].roomBottom - mapSplit[i].roomTop - ratioY) / 2.0f);
                    moveX = Mathf.FloorToInt((mapSplit[i].roomRight - mapSplit[i].roomLeft - ratioX) / 2.0f);
                    mapSplit[i].roomTop = mapSplit[i].roomTop + moveY;
                    mapSplit[i].roomBottom = mapSplit[i].roomTop + ratioY;
                    mapSplit[i].roomLeft = mapSplit[i].roomLeft + moveX;
                    mapSplit[i].roomRight = mapSplit[i].roomLeft + ratioX;

                    for (int j = 0; j < ratioY; j++)
                    {
                        for (int k = 0; k < ratioX; k++)
                        {
                            dungeonMap[mapSplit[i].roomLeft + k + 1, mapSplit[i].roomTop + j + 1] = (int)ID.ROOM;
                        }
                    }
                }
                else
                    break;
            }
        }
    }
}