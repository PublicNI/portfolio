﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dungeon;
using static Dungeon.DungeonInfo;   // 変数はDungeonInfoから

namespace Dungeon
{
    public class DungeonGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject wall = default;
        [SerializeField] private GameObject brightFloor = default;
        [SerializeField] private GameObject darkFloor = default;
        [SerializeField] private DungeonRoomCreate dungeonRoomCreate = default;
        [SerializeField] private DungeonRoadCreate dungeonRoadCreate = default;

        public void Generate()
        {
            dungeonRoomCreate.InitialMapData();
            dungeonRoomCreate.MapSplit();
            dungeonRoomCreate.CreateRoom();
            dungeonRoadCreate.CreateRoad();
            WallLayout();
            //Test();
        }

        /// <summary>
        /// テキストファイルで配列の中身を確認する
        /// </summary>
        public void Test()
        {
            int count = 0;
            string newLine = "";

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (count % (width + height) / 2 == 0 && count != 0)
                    {
                        newLine += "\n";
                    }
                    newLine += dungeonMap[i, j].ToString();

                    count++;
                }
            }
            System.IO.File.WriteAllText(@"C:\TechStadium\ts_un_stu_2020_01\90_FinalProject\10_UnityProject\test.txt", string.Join(",", newLine));
        }

        /// <summary>
        /// マップデータを元にオブジェクトを配置
        /// </summary>
        private void WallLayout()
        {
            GameObject dungeonFloor;
            GameObject dungeonWall;
            GameObject dungeonOutSideWall;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    // マス目を分かりやすくする為、色違いで交互に配置
                    if ((i + j) % 2 == 0)
                    {
                        dungeonFloor = Instantiate(brightFloor, new Vector3(i - width / 2, 0, j - height / 2), Quaternion.identity);
                    }
                    else
                    {
                        dungeonFloor = Instantiate(darkFloor, new Vector3(i - width / 2, 0, j - height / 2), Quaternion.identity);
                    }

                    dungeonFloor.transform.parent = GameObject.FindWithTag("Dungeon").transform;

                    if (dungeonMap[i, j] == (int)ID.WALL)
                    {
                        for (int k = 0; k < 1; k++)
                        {
                            // Dungeonタグのオブジェクトを起点にして配置する
                            dungeonWall = Instantiate(wall, new Vector3(i - width / 2, 1, j - height / 2), Quaternion.identity);
                            dungeonWall.transform.parent = GameObject.FindWithTag("Dungeon").transform;
                        }
                    }
                }
            }

            // 外壁を作る
            for (int i = -1; i < height + 1; i++)
            {
                for (int j = 0; j < 1 + 1; j++)
                {
                    dungeonOutSideWall = Instantiate(wall, new Vector3(-1 - width / 2, j, i - height / 2), Quaternion.identity);
                    dungeonOutSideWall.transform.parent = GameObject.FindWithTag("Dungeon").transform;

                    dungeonOutSideWall = Instantiate(wall, new Vector3(width - width / 2, j, i - height / 2), Quaternion.identity);
                    dungeonOutSideWall.transform.parent = GameObject.FindWithTag("Dungeon").transform;
                }

            }
            for (int i = -1; i < width; i++)
            {
                for (int j = 0; j < 1 + 1; j++)
                {
                    dungeonOutSideWall = Instantiate(wall, new Vector3(i - width / 2, j, -1 - height / 2), Quaternion.identity);
                    dungeonOutSideWall.transform.parent = GameObject.FindWithTag("Dungeon").transform;

                    dungeonOutSideWall = Instantiate(wall, new Vector3(i - width / 2, j, height - height / 2), Quaternion.identity);
                    dungeonOutSideWall.transform.parent = GameObject.FindWithTag("Dungeon").transform;
                }
            }
        }
    }
}