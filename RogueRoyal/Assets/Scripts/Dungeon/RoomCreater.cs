﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Dungeon.DungeonInfo;

public class RoomCreater : MonoBehaviour
{

    /// <summary>
    /// 全面が壁のデータを作成
    /// </summary>
    public void AllWallDataCreate()
    {
        dungeonMap = new int[width, height];

        for(int x = 0; x < width; x++)
        {
            for(int y = 0; y < height; y++)
            {
                dungeonMap[x, y] = (int)ID.WALL;
            }
        }
    }

    /// <summary>
    /// 部屋を作る為にマップをいくつかのエリアに分割
    /// </summary>
    public void MapSplit()
    {

    }
}
