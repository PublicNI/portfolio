﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Dungeon.DungeonInfo;

namespace Enemy
{
    public class EnemyManager : MonoBehaviour
    {
        [SerializeField] private GameObject enemy = default;

        public static List<EnemyInfo> enemyInfos = new List<EnemyInfo>();

        public static int enemyNum = 19;
        private string enemyName;
        GameObject[] tmpArray;

        /// <summary>
        /// エネミーを管理するリストの初期化
        /// </summary>
        public void SetEnemy()
        {
            for (int i = 0; i < enemyNum; i++)
            {
                enemyInfos.Add(new EnemyInfo());
            }
        }

        /// <summary>
        /// エネミーをどこかの部屋にスポーンさせる
        /// </summary>
        public void EnemySporn()
        {
            int x;
            int y;

            Quaternion rote = Quaternion.Euler(45.0f, 0, 45.0f);
            GameObject enemyObj;

            while (enemyNum > 0)
            {
                x = Random.Range(0, width);
                y = Random.Range(0, height);

                if (dungeonMap[x, y] >= (int)ID.ROOM)
                {
                    enemyObj = Instantiate(enemy, new Vector3(x - width / 2, 1, y - height / 2), rote);
                    enemyObj.transform.parent = GameObject.FindWithTag("EnemyBox").transform;

                    enemyNum--;
                }
            }
        }

        /// <summary>
        /// 配置したエネミーをリストに格納
        /// </summary>
        public void GetEnemyID()
        {
            GameObject[] tmpArray = GameObject.FindGameObjectsWithTag("Enemy");

            for (int i = 0; i < tmpArray.Length; i++)
            {
                enemyInfos[i].enemyID = tmpArray[i];
            }
        }
    }
}