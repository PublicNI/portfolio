﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Dungeon.DungeonInfo;   // dungeonMap変数はDungeonInfoから
using static Enemy.EnemyManager;
using Battle;
using static Item.GetDatabase;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Slider timingSlider = default;
        [SerializeField] private EnemyWeaponHitRange enemyWeaponHitRange = default;

        private string[] enemyInputKey = new string[19];
        private string[] enemyBeforeInputKey = new string[19];
        private Vector3 enemyOriginalPosition;
        private bool enemyActionCount = false;
        private bool enemyGaugeMaxCount = false;
        private int judgKey;

        private float enemyTimingSliderValue;

        // 現在地からそれぞれの方向に進む為の値を格納する
        Vector3 enemyMoveXPlus;
        Vector3 enemyMoveXMinus;
        Vector3 enemyMoveYPlus;
        Vector3 enemyMoveYMinus;

        public void EGet(Slider slider)
        {
            enemyTimingSliderValue = slider.value;
        }

        /// <summary>
        /// スライダーから受け取った値に沿って行動させる
        /// </summary>
        /// <param name="timingSlider"></param>
        public void EnemyAI()
        {
            if (enemyTimingSliderValue == timingSlider.maxValue - 10)
            {
                enemyGaugeMaxCount = true;
            }

            if (enemyActionCount == false)
            {
                for (int i = 0; i < enemyInfos.Count; i++)
                {
                    // プレイヤーのスクリプトを元に、入力をランダムで行うようにする
                    judgKey = Random.Range(0, 4);

                    switch (judgKey)
                    {
                        case 0:
                            enemyInputKey[i] = "w";
                            break;
                        case 1:
                            enemyInputKey[i] = "a";
                            break;
                        case 2:
                            enemyInputKey[i] = "s";
                            break;
                        case 3:
                            enemyInputKey[i] = "d";
                            break;
                    }

                    switch (enemyInputKey[i])
                    {
                        case "w":
                            break;
                        case "a":
                            break;
                        case "s":
                            break;
                        case "d":
                            break;
                    }

                    enemyActionCount = true;
                }
            }
            else if (enemyTimingSliderValue>= timingSlider.minValue && enemyGaugeMaxCount == true)
            {
                for(int i= 0; i < enemyInfos.Count; i++)
                {
                    if (enemyActionCount == true)
                    {
                        MovingEnemy(i);

                        //HitBoxTurnJudg(i);
                    }

                    EnemyHitWall(i);
                }

                for (int i = 0; i < enemyInfos.Count; i++)
                {
                    enemyInputKey[i] = "";
                }

                enemyActionCount = false;
                enemyGaugeMaxCount = false;
            }
        }

        /// <summary>
        /// WASDでそれぞれの方向に移動
        /// </summary>
        private void MovingEnemy(int ID)
        {
            enemyMoveXPlus = new Vector3(enemyInfos[ID].enemyID.transform.position.x + 1, enemyInfos[ID].enemyID.transform.position.y, enemyInfos[ID].enemyID.transform.position.z);
            enemyMoveXMinus = new Vector3(enemyInfos[ID].enemyID.transform.position.x - 1, enemyInfos[ID].enemyID.transform.position.y, enemyInfos[ID].enemyID.transform.position.z);
            enemyMoveYPlus = new Vector3(enemyInfos[ID].enemyID.transform.position.x, enemyInfos[ID].enemyID.transform.position.y, enemyInfos[ID].enemyID.transform.position.z + 1);
            enemyMoveYMinus = new Vector3(enemyInfos[ID].enemyID.transform.position.x, enemyInfos[ID].enemyID.transform.position.y, enemyInfos[ID].enemyID.transform.position.z - 1);

            // 壁に当たったら元の位置に戻す為、元の位置の座標を保存しておく
            enemyOriginalPosition = enemyInfos[ID].enemyID.transform.position;

            switch (enemyInputKey[ID])
            {
                case "w":
                    enemyInfos[ID].enemyID.transform.position = Vector3.Lerp(enemyInfos[ID].enemyID.transform.position, enemyMoveYPlus, 1);
                    break;
                case "a":
                    enemyInfos[ID].enemyID.transform.position = Vector3.Lerp(enemyInfos[ID].enemyID.transform.position, enemyMoveXMinus, 1);
                    break;
                case "s":
                    enemyInfos[ID].enemyID.transform.position = Vector3.Lerp(enemyInfos[ID].enemyID.transform.position, enemyMoveYMinus, 1);
                    break;
                case "d":
                    enemyInfos[ID].enemyID.transform.position = Vector3.Lerp(enemyInfos[ID].enemyID.transform.position, enemyMoveXPlus, 1);
                    break;
            }
        }

        /// <summary>
        /// 配列上壁の位置に移動したら元の位置に戻す
        /// </summary>
        private void EnemyHitWall(int enemyID)
        {
            if (dungeonMap[(int)(enemyInfos[enemyID].enemyID.transform.position.x + width / 2), (int)(enemyInfos[enemyID].enemyID.transform.position.z + height / 2)] == (int)ID.WALL)
            {
                enemyInfos[enemyID].enemyID.transform.position = Vector3.Lerp(enemyInfos[enemyID].enemyID.transform.position, enemyOriginalPosition, 1);
            }
        }

        /// <summary>
        /// 攻撃範囲を回転する時の判定
        /// </summary>
        private void HitBoxTurnJudg(int ID)
        {
            if (enemyBeforeInputKey != enemyInputKey)
            {
                switch (enemyBeforeInputKey[ID])
                {
                    case "w":
                        switch (enemyInputKey[ID])
                        {
                            case "a":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 270.0f);
                                break;
                            case "s":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 180.0f);
                                break;
                            case "d":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 90.0f);
                                break;
                        }
                        break;
                    case "a":
                        switch (enemyInputKey[ID])
                        {
                            case "w":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 90.0f);
                                break;
                            case "s":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 270.0f);
                                break;
                            case "d":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 180.0f);
                                break;
                        }
                        break;
                    case "s":
                        switch (enemyInputKey[ID])
                        {
                            case "w":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 180.0f);
                                break;
                            case "a":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 90.0f);
                                break;
                            case "d":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 270.0f);
                                break;
                        }
                        break;
                    case "d":
                        switch (enemyInputKey[ID])
                        {
                            case "w":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 270.0f);
                                break;
                            case "a":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 180.0f);
                                break;
                            case "s":
                                enemyWeaponHitRange.EnemyRotateHitBox(ID, 90.0f);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            // 攻撃範囲をプレーヤーの進行方向に合わせて動かす為に、一つ前の入力を保存しておく
            if (enemyInputKey[ID] != "")
            {
                enemyBeforeInputKey = enemyInputKey;
            }
        }

        public void SetBeforeInputKey()
        {
            for(int i = 0; i < 19; i++)
            {
                enemyBeforeInputKey[i] = "w";
            }
        }
    }
}