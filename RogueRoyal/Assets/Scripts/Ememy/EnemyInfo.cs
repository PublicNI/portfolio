﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    /// <summary>
    /// プレイヤーのパラメータ
    /// </summary>
    public class EnemyInfo
    {
        public GameObject enemyID;
        public int enemyHP = 10;
        public int weaponATK;
        public GameObject[] weaponRange = new GameObject[6];
    }
}