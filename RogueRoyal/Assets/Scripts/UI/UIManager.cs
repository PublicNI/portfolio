﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Slider timingSlider = default;
    [SerializeField] private Image hitImage = default;

    private float changeValue = 1.0f;
    private float hitValue = 15.0f;
    private float timeOut = 0.01f;
    private float timeElapsed = 0.0f;

    void FixedUpdate()
    {
            MovingGauge();
    }

    /// <summary>
    /// ゲージを動かす
    /// </summary>
    void MovingGauge()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed >= timeOut)
        {
            timingSlider.value += changeValue;

            if (timingSlider.value >= timingSlider.maxValue)
            {
                timingSlider.value = 0;
            }
            if (timingSlider.value >= hitValue - 4 && timingSlider.value <= hitValue + 4)
            {
                hitImage.color = new Color(255, 0, 0, 0.1f);
            }
            if (timingSlider.value < hitValue - 4 || timingSlider.value > hitValue + 4)
            {
                hitImage.color = new Color(255, 255, 255, 0.1f);
            }

            timeElapsed = 0.0f;
        }
    }
}