﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    [SerializeField] private Text timeText = default;
    [HideInInspector] public static float timeCount = 120.0f;

    private void Update()
    {
        TimeLimitViewer();
    }

    /// <summary>
    /// 制限時間を表示する
    /// </summary>
    void TimeLimitViewer()
    {
        if(timeCount > 0.0f)
        {
            timeCount -= Time.deltaTime;

            timeText.text = "TimeLimit : "
                            + System.Math.Floor(timeCount).ToString()
                            + "s";
        }
    }
}
