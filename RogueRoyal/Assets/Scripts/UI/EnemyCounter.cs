﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Enemy.EnemyManager;

public class EnemyCounter : MonoBehaviour
{
    [SerializeField] private Text enemyCount = default;

    int tmp;

    private void Update()
    {
        EnemyNumViewer();
    }

    /// <summary>
    /// 制限時間を表示する
    /// </summary>
    void EnemyNumViewer()
    {
        tmp = 19 - enemyNum;

        enemyCount.text = "Enemy : " + tmp;
    }
}
