﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{
    [SerializeField] private Button[] buttons = default; 
    [SerializeField] private Image image = default; 

    void Start()
    {
        buttons[0].onClick.AddListener(StartButton);
        buttons[1].onClick.AddListener(TutorialButton);
        buttons[2].onClick.AddListener(BackButton);
    }

    private void StartButton()
    {
        SceneManager.LoadScene("Main");
    }

    private void TutorialButton()
    {
        buttons[2].gameObject.SetActive(true);
        image.gameObject.SetActive(true);
    }

    private void BackButton()
    {
        buttons[2].gameObject.SetActive(false);
        image.gameObject.SetActive(false);
    }
}
