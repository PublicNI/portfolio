﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Dungeon;
using Player;
using Battle;
using Item;
using Enemy;
using UnityEngine.SceneManagement;
using static Item.GetDatabase;
using static Enemy.EnemyManager;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int[] ID = null;
    [SerializeField] private Button[] button = null;
    [SerializeField] private Text gameOver = default;
    [SerializeField] private Image loadImage = default;
    //[SerializeField] private Slider actionTimingGauge = default;
    [SerializeField] private DungeonGenerator dungeonGenerator =  default;
    [SerializeField] private PlayerGenerator playerGenerator = default;
    [SerializeField] private PlayerManager playerManager = default;
    [SerializeField] private ItemGenerator itemGenerator = default;
    [SerializeField] private PlayerWeaponHitRange playerWeaponHitRange = default;
    [SerializeField] private EnemyWeaponHitRange enemyWeaponHitRange = default;
    [SerializeField] private PlayerController playerController = default;
    [SerializeField] private EnemyManager enemyManager = default;
    [SerializeField] private EnemyController enemyController = default;
    //[SerializeField] private TimeCounter timeCounter = default;

    private bool judg = false;

    IEnumerator Start()
    {
        loadImage.gameObject.SetActive(true);

        // データベースを読み込み終わるまで待機
        yield return StartCoroutine("StartJudg");

        dungeonGenerator.Generate();
        playerGenerator.Generate();

        playerManager.SetPlayer();
        playerManager.GetPlayerID();
        playerGenerator.MovingCamera(ID[0]);

        //itemGenerator.ItemSporn();

        enemyManager.SetEnemy();
        enemyManager.EnemySporn();
        enemyManager.GetEnemyID();

        playerWeaponHitRange.PlayerSetHitBox(ID[0]);
        //for (int i = 0; i < EnemyManager.enemyInfos.Count; i++)
        //{
        //    enemyWeaponHitRange.EnemySetHitBox(i);
        //}
        playerController.SetBeforeInputKey();
        enemyController.SetBeforeInputKey();

        button[0].onClick.AddListener(Restart);
        button[1].onClick.AddListener(TitleBack);

        loadImage.gameObject.SetActive(false);
        // ゲーム開始判定ON
        judg = true;
    }

    IEnumerator StartJudg()
    {
        while(GetDatabase.startJudg == false)
        {
            yield return new WaitForSeconds(1f);
        }
    }

    private void Update()
    {
        if (startJudg == true)
        {
            if (TimeCounter.timeCount <= 0.0f)
            {
                StartCoroutine(WaitGameOver());
            }

            // 一時的措置
            if (enemyNum >= enemyInfos.Count && judg == true)
            {
                Debug.Log(enemyNum);
                SceneManager.LoadScene("Result");
            }
        }
    }

    IEnumerator WaitGameOver()
    {
        gameOver.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);

        Initialization();

        SceneManager.LoadScene("Title");

    }

    private void FixedUpdate()
    {
        if(startJudg == true)
        {
            playerController.GetSliderValue();
            enemyController.EnemyAI();
        }
    }

    /// <summary>
    /// シーンの再読み込み
    /// </summary>
    private void Restart()
    {
        Initialization();

        Scene loadScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadScene.name);
    }

    /// <summary>
    /// タイトルに戻る
    /// </summary>
    private void TitleBack()
    {
        Initialization();

        SceneManager.LoadScene("Title");
    }

    private void Initialization()
    {
        GetDatabase.startJudg = false;
        enemyNum = 19;
    }
}