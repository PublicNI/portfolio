﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Item.GetDatabase;
using static Dungeon.DungeonInfo;
using static Enemy.EnemyManager;

namespace Battle
{
    public class EnemyWeaponHitRange : MonoBehaviour
    {
        [SerializeField] private GameObject enemyHitBox = default;

        GameObject[] enemyHitBoxObj = new GameObject[10];

        /// <summary>
        /// エネミーに追従するヒットボックスを配置
        /// </summary>
        /// <param name="ID">エネミーの番号</param>
        public void EnemySetHitBox(int ID)
        {
            //if (itemInfos[itemID].itemName == "ShortSowrd")
            //{
            //    for (int i = 0; i < itemInfos[itemID].rangeType; i++)
            //    {
            //        enemyHitBoxObj[i] = Instantiate(enemyHitBox, new Vector3(enemyInfos[ID].enemyID.transform.position.x, 0.6f, enemyInfos[ID].enemyID.transform.position.z + i + 1), Quaternion.identity);

            //        // エネミーを親に指定
            //        enemyHitBoxObj[i].transform.parent = enemyInfos[ID].enemyID.transform;

            //        // エネミーの攻撃力をアイテムの攻撃力に設定
            //        enemyInfos[ID].weaponATK = itemInfos[itemID].itemATK;
            //    }
            //}
            //else if (itemInfos[itemID].itemName == "Spear")
            //{
            //    for (int i = 0; i < itemInfos[itemID].rangeType; i++)
            //    {
            //        enemyHitBoxObj[i] = Instantiate(enemyHitBox, new Vector3(enemyInfos[ID].enemyID.transform.position.x, 0.6f, enemyInfos[ID].enemyID.transform.position.z + i + 1), Quaternion.identity);

            //        // エネミーを親に指定
            //        enemyHitBoxObj[i].transform.parent = enemyInfos[ID].enemyID.transform;
            //    }
            //}

            for (int i = 0; i < 3; i++)
            {
                enemyHitBoxObj[i] = Instantiate(enemyHitBox, new Vector3(enemyInfos[ID].enemyID.transform.position.x, 0.6f, enemyInfos[ID].enemyID.transform.position.z + i + 1), Quaternion.identity);

                // アイテムを取得したエネミーのヒットボックスのみを操作できるようにエネミーとプレハブを紐づける
                enemyInfos[ID].weaponRange[i] = enemyHitBoxObj[i];

                // エネミーを親に指定
                enemyHitBoxObj[i].transform.parent = enemyInfos[ID].enemyID.transform;
            }

            // エネミーの攻撃力をアイテムの攻撃力に設定
            enemyInfos[ID].weaponATK = 2;
        }

        /// <summary>
        /// エネミーの向きに合わせて攻撃範囲も回転させる
        /// </summary>
        /// <param name="angle"></param>
        public void EnemyRotateHitBox(int ID, float angle)
        {
            for (int i = 0; i < 3; i++)
            {
                enemyHitBoxObj[i].transform.RotateAround(enemyInfos[ID].enemyID.transform.position, Vector3.up, angle);
                Debug.Log(enemyHitBoxObj[i]);
            }
        }
    }
}