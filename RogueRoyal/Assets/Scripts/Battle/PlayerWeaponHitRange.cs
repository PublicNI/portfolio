﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Player.PlayerManager;
using static Item.GetDatabase;

namespace Battle
{
    public class PlayerWeaponHitRange : MonoBehaviour
    {
        [SerializeField] private GameObject hitBox = default;
        [SerializeField] private Text weaponText = default;

        private GameObject[] hitBoxObj;

        private int i = 0;
        private int j = 0;
        private int itemID;
        private int hitBoxNum;

        /// <summary>
        /// プレイヤーに追従するヒットボックスを配置
        /// </summary>
        /// <param name="ID">プレイヤーの番号</param>
        public void PlayerSetHitBox(int ID)
        {
            itemID = Random.Range(0, itemInfos.Count);
            hitBoxObj = new GameObject[itemInfos[itemID].rangeType];
            
            switch (itemID)
            {
                case 0:
                    for (i = 0; i < itemInfos[itemID].rangeType; i++)
                    {
                        hitBoxObj[i] = Instantiate(hitBox, new Vector3(playerInfos[ID].playerID.transform.position.x,
                                                                        0.6f,
                                                                        playerInfos[ID].playerID.transform.position.z + i + 1), Quaternion.identity);
                    }
                    break;
                case 1:
                    for (i = 0; i < itemInfos[itemID].rangeType; i++)
                    {
                        hitBoxObj[i] = Instantiate(hitBox, new Vector3(playerInfos[ID].playerID.transform.position.x,
                                                                       0.6f,
                                                                       playerInfos[ID].playerID.transform.position.z + i + 1), Quaternion.identity);
                    }
                    break;
                case 2:
                    for (i = 0; i < itemInfos[itemID].rangeType; i++)
                    {
                        if (i % 2 == 0) j++;
                        else j--;

                        hitBoxObj[i] = Instantiate(hitBox, new Vector3(playerInfos[ID].playerID.transform.position.x + j,
                                                                       0.6f,
                                                                       playerInfos[ID].playerID.transform.position.z + i + 1), Quaternion.identity);
                    }
                    break;
                case 3:
                    for (i = 0; i < itemInfos[itemID].rangeType; i++)
                    {
                        hitBoxObj[i] = Instantiate(hitBox, new Vector3(playerInfos[ID].playerID.transform.position.x + i - 1,
                                                                       0.6f,
                                                                       playerInfos[ID].playerID.transform.position.z + 1), Quaternion.identity);
                    }
                    break;
            }

            for (i = 0; i < itemInfos[itemID].rangeType; i++)
            {
                // アイテムを取得したプレイヤーのヒットボックスのみを操作できるようにプレイヤーとプレハブを紐づける
                playerInfos[ID].weaponRange[i] = hitBoxObj[i];
                hitBoxNum = hitBoxObj.Length;

                // プレイヤーを親に指定
                hitBoxObj[i].transform.parent = playerInfos[ID].playerID.transform;

                // プレイヤーの攻撃力をアイテムの攻撃力に設定
                playerInfos[ID].weaponATK = itemInfos[itemID].itemATK;

                weaponText.text = itemInfos[itemID].itemName;
            }
        }

        /// <summary>
        /// プレイヤーの向きに合わせて攻撃範囲も回転させる
        /// </summary>
        /// <param name="angle"></param>
        public void PlayerRotateHitBox(int ID, float angle)
        {
            for (int i = 0; i < hitBoxObj.Length; i++)
            {
                playerInfos[ID].weaponRange[i].transform.RotateAround(playerInfos[ID].playerID.transform.position, Vector3.up, angle);
            }
        }
    }
}

//if (itemInfos[itemID].itemName == "ShortSword")
//{
//    for (int i = 0; i < itemInfos[itemID].rangeType; i++)
//    {
//        hitBoxObj[i] = Instantiate(hitBox, new Vector3(playerInfos[ID].playerID.transform.position.x, 0.6f, playerInfos[ID].playerID.transform.position.z + i + 1), Quaternion.identity);

//        // アイテムを取得したプレイヤーのヒットボックスのみを操作できるようにプレイヤーとプレハブを紐づける
//        playerInfos[ID].weaponRange[i] = hitBoxObj[i];

//        // プレイヤーを親に指定
//        hitBoxObj[i].transform.parent = playerInfos[ID].playerID.transform;
//    }

//    // プレイヤーの攻撃力をアイテムの攻撃力に設定
//    playerInfos[ID].weaponATK = itemInfos[itemID].itemATK;
//}
//else if (itemInfos[itemID].itemName == "Spear")
//{
//    for (int i = 0; i < itemInfos[itemID].rangeType; i++)
//    {
//        hitBoxObj[i] = Instantiate(hitBox, new Vector3(playerInfos[ID].playerID.transform.position.x, 0.6f, playerInfos[ID].playerID.transform.position.z + i + 1), Quaternion.identity);

//        hitBoxObj[i].transform.parent = playerInfos[ID].playerID.transform;
//    }
//}