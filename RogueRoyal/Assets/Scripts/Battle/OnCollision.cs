﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollision : MonoBehaviour
{
    public static bool hitEnemy = false;
    public static Collision attackObj;

    private void OnCollisionEnter(Collision collision)
    {
        if (this.gameObject.tag == "HitBox" && collision.gameObject.tag == "Enemy")
        {
            attackObj = collision;

            hitEnemy = true;
            Debug.Log("あたった");
        }
    }
}