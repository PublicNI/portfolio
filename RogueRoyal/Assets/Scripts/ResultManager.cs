﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultManager : MonoBehaviour
{
    [SerializeField] private Text scoreText = default;

    private void Start()
    {
        scoreText.text = "ClearTime " + System.Math.Floor(TimeCounter.timeCount - 120).ToString() + "s";

        StartCoroutine(WaitTitleLoad());
    }

    IEnumerator WaitTitleLoad()
    {
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene("Title");
    }
}
