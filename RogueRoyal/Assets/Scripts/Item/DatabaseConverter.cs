﻿using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using System;
using UnityEngine;

namespace Item
{
    /// <summary>
    /// アイテムに関する情報を格納
    /// </summary>
    public class ItemInfo
    {
        public int itemID { get; set; }
        public string itemName { get; set; }
        public int itemATK { get; set; }
        public int rangeType { get; set; }
        public int rarity { get; set; }

        public ItemInfo()
        {
            itemID = 0;
            itemName = "";
            itemATK = 0;
            rangeType = 0;
            rarity = 0;
        }
    }

    public class DatabaseConverter
    {
        /// <summary>
        /// Deserialize from json.
        /// ItemInfo型のリストがjsonに入っていると仮定して
        /// </summary>
        /// <returns>The from json.</returns>
        /// <param name="sStrJson">S string json.</param>
        public static List<ItemInfo> DeserializeFromJson(string sStrJson)
        {
            var ret = new List<ItemInfo>();

            // JSONデータは最初は配列から始まるので、Deserialize（デコード）した直後にリストへキャスト      
            IList jsonList = (IList)Json.Deserialize(sStrJson);

            // リストの内容はオブジェクトなので、辞書型の変数に一つ一つ代入しながら、処理
            foreach (IDictionary jsonOne in jsonList)
            {
                //新レコード解析開始

                var tmp = new ItemInfo();

                //該当するキー名が jsonOne に存在するか調べ、存在したら取得して変数に格納する。
                if (jsonOne.Contains("Id"))
                {
                    tmp.itemID = Convert.ToInt32(jsonOne["Id"]);
                }
                if (jsonOne.Contains("Name"))
                {
                    tmp.itemName = (string)jsonOne["Name"];
                }
                if (jsonOne.Contains("ATK"))
                {
                    tmp.itemATK = Convert.ToInt32(jsonOne["ATK"]);
                }
                if (jsonOne.Contains("RangeType"))
                {
                    tmp.rangeType = Convert.ToInt32(jsonOne["RangeType"]);
                }
                if (jsonOne.Contains("rarity"))
                {
                    tmp.rarity = Convert.ToInt32(jsonOne["rarity"]);
                }

                //現レコード解析終了
                ret.Add(tmp);
            }

            return ret;
        }
    }
}