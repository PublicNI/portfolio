﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Dungeon.DungeonInfo;   // マップに関する変数はDungeonInfoから

namespace Item
{
    public class ItemGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject[] item = default;

        /// <summary>
        /// アイテムをどこかの部屋にスポーンさせる
        /// </summary>
        public void ItemSporn()
        {
            int x;
            int y;
            int itemID;
            int itemNum = 0;

            Quaternion rote = Quaternion.Euler(45.0f, 0, 0);
            GameObject itemObj;

            while(itemNum <= 30)
            {
                x = Random.Range(0, width);
                y = Random.Range(0, height);

                if (dungeonMap[x, y] >= (int)ID.ROOM)
                {
                    itemID = Random.Range(0, 4);

                    itemObj = Instantiate(item[itemID], new Vector3(x - width / 2, 1, y - height / 2), rote);
                    itemObj.transform.parent = GameObject.FindWithTag("ItemBox").transform;

                    itemNum++;
                }
            }
        }
    }
}