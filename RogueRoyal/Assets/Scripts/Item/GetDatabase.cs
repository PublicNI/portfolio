﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

namespace Item
{
    public class GetDatabase : MonoBehaviour
    {
        public static List<ItemInfo> itemInfos = new List<ItemInfo>();
        public static bool startJudg = false;

        public void Start()
        {
            OnClickGetJsonFromWebRequest();
        }

        /// <summary>
        /// Raises the click get json from www event.
        /// </summary>
        public void OnClickGetJsonFromWebRequest()
        {
            GetJsonFromWebRequest();
        }

        /// <summary>
        /// Gets the json from www.
        /// </summary>
        private void GetJsonFromWebRequest()
        {
            // Wwwを利用して json形式のデータ取得をリクエストする
            StartCoroutine(
                DownloadJson(
                    CallbackWebRequestSuccess, // APIコールが成功した際に呼ばれる関数を指定
                    CallbackWebRequestFailed // APIコールが失敗した際に呼ばれる関数を指定
                )
            );
        }

        /// <summary>
        /// デコードしてitemInfosに格納する
        /// </summary>
        /// <param name="response"></param>
        private void CallbackWebRequestSuccess(string response)
        {
            //Json の内容を ItemInfo型のリストとしてデコードする。
            itemInfos = DatabaseConverter.DeserializeFromJson(response);

            startJudg = true;
        }

        /// <summary>
        /// Callbacks the www failed.
        /// </summary>
        private void CallbackWebRequestFailed()
        {
            // jsonデータ取得に失敗した
            Debug.Log("WebRequest Failed");
        }

        /// <summary>
        /// Downloads the json.
        /// </summary>
        /// <returns>The json.</returns>
        /// <param name="cbkSuccess">Cbk success.</param>
        /// <param name="cbkFailed">Cbk failed.</param>
        private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
        {
            UnityWebRequest www = UnityWebRequest.Get("http://localhost:80/Roguelike/Weapon/getMessages");
            yield return www.SendWebRequest();
            if (www.error != null)
            {
                //レスポンスエラーの場合
                Debug.LogError(www.error);
                if (null != cbkFailed)
                {
                    cbkFailed();
                }
            }
            else if (www.isDone)
            {
                // リクエスト成功の場合
                if (null != cbkSuccess)
                {
                    cbkSuccess(www.downloadHandler.text);
                }
            }
        }
    }
}