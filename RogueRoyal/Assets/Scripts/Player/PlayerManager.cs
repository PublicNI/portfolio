﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Item;

namespace Player
{
    public class PlayerManager : MonoBehaviour
    {
        public static List<PlayerInfo> playerInfos = new List<PlayerInfo>();

        /// <summary>
        /// プレイヤーを管理するリストの初期化
        /// </summary>
        public void SetPlayer()
        {
            for (int playerNum = 0; playerNum < 20; playerNum++)
            {
                playerInfos.Add(new PlayerInfo());
            }
        }

        /// <summary>
        /// 配置したプレイヤーをリストに格納
        /// </summary>
        public void GetPlayerID()
        {
            GameObject[] tmpArray = GameObject.FindGameObjectsWithTag("Player");

            for (int i = 0; i < tmpArray.Length; i++)
            {
                playerInfos[i].playerID = tmpArray[i];
            }
        }
    }
}
