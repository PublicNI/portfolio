﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Dungeon.DungeonInfo;   // dungeonMap変数はDungeonInfoから
using static Player.PlayerManager;
using Battle;
using Enemy;
using static Item.GetDatabase;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Slider timingSlider = default;
        [SerializeField] private PlayerWeaponHitRange playerWeaponHitRange = default;
        [SerializeField] private CameraShaker cameraShaker = default;
        [SerializeField] private int[] playerID = null;

        private string inputKey;
        private string beforeInputKey;
        private Vector3 originalPosition;
        private bool actionCount = false;
        private bool gaugeMaxCount = false;
        private bool standBy = false;
        private float hitValue = 15.0f;

        private float timingSliderValue;

        // 現在地からそれぞれの方向に進む為の値を格納する
        Vector3 moveXPlus;
        Vector3 moveXMinus;
        Vector3 moveYPlus;
        Vector3 moveYMinus;

        public void Get(Slider slider)
        {
            timingSliderValue = slider.value;
        }

        /// <summary>
        /// スライダーから受け取った値に沿って行動させる
        /// </summary>
        /// <param name="timingSlider"></param>
        public void GetSliderValue()
        {
            if (timingSliderValue == timingSlider.maxValue - 10)
            {
                gaugeMaxCount = true;
            }
            if (Input.anyKeyDown && actionCount == false)
            {
                if (timingSliderValue >= hitValue - 4 && timingSliderValue <= hitValue + 4)
                {
                    inputKey = Input.inputString;

                    switch (inputKey)
                    {
                        case "w":
                            break;
                        case "a":
                            break;
                        case "s":
                            break;
                        case "d":
                            break;
                        default:
                            inputKey = "";
                            break;
                    }
                }
                else
                {
                    Debug.Log("タイミングが違います");
                }

                actionCount = true;
            }
            else if (timingSliderValue >= timingSlider.minValue && gaugeMaxCount == true)
            {
                if (actionCount == true)
                {
                    if (OnCollision.hitEnemy == true)
                    {
                        AttackPlayer();
                        OnCollision.hitEnemy = false;

                        HitBoxTurnJudg(playerID[0]);
                    }
                    else
                    {
                        MovingPlayer(playerID[0]);

                        HitBoxTurnJudg(playerID[0]);

                        if (OnCollision.hitEnemy == true)
                        {
                            standBy = true;
                        }
                    }
                }

                HitWall(playerID[0]);

                inputKey = "";
                actionCount = false;
                gaugeMaxCount = false;

                //if(standBy == true)
                //{
                //    OnCollision.hitEnemy = false;
                //}
            }
        }

        /// <summary>
        /// WASDでそれぞれの方向に移動
        /// </summary>
        private void MovingPlayer(int ID)
        {
            moveXPlus = new Vector3(playerInfos[ID].playerID.transform.position.x + 1, playerInfos[ID].playerID.transform.position.y, playerInfos[ID].playerID.transform.position.z);
            moveXMinus = new Vector3(playerInfos[ID].playerID.transform.position.x - 1, playerInfos[ID].playerID.transform.position.y, playerInfos[ID].playerID.transform.position.z);
            moveYPlus = new Vector3(playerInfos[ID].playerID.transform.position.x, playerInfos[ID].playerID.transform.position.y, playerInfos[ID].playerID.transform.position.z + 1);
            moveYMinus = new Vector3(playerInfos[ID].playerID.transform.position.x, playerInfos[ID].playerID.transform.position.y, playerInfos[ID].playerID.transform.position.z - 1);

            // 壁に当たったら元の位置に戻す為、元の位置の座標を保存しておく
            originalPosition = playerInfos[ID].playerID.transform.position;

            switch (inputKey)
            {
                case "w":
                    playerInfos[ID].playerID.transform.position = Vector3.Lerp(playerInfos[ID].playerID.transform.position, moveYPlus, 1);
                    cameraShaker.Shake(0.12f, 0.07f);
                    break;
                case "a":
                    playerInfos[ID].playerID.transform.position = Vector3.Lerp(playerInfos[ID].playerID.transform.position, moveXMinus, 1);
                    cameraShaker.Shake(0.12f, 0.07f);
                    break;
                case "s":
                    playerInfos[ID].playerID.transform.position = Vector3.Lerp(playerInfos[ID].playerID.transform.position, moveYMinus, 1);
                    cameraShaker.Shake(0.12f, 0.07f);
                    break;
                case "d":
                    playerInfos[ID].playerID.transform.position = Vector3.Lerp(playerInfos[ID].playerID.transform.position, moveXPlus, 1);
                    cameraShaker.Shake(0.12f, 0.07f);
                    break;
            }
        }

        /// <summary>
        /// WASDでそれぞれの方向に攻撃
        /// </summary>
        private void AttackPlayer()
        {
            if (beforeInputKey == inputKey)
            {
                    OnCollision.attackObj.gameObject.SetActive(false);
                    cameraShaker.Shake(0.12f, 0.07f);

                    EnemyManager.enemyNum++;
            }
        }

        /// <summary>
        /// 配列上壁の位置に移動したら元の位置に戻す
        /// </summary>
        private void HitWall(int playerID)
        {
            if (dungeonMap[(int)(playerInfos[playerID].playerID.transform.position.x + width / 2), (int)(playerInfos[playerID].playerID.transform.position.z + height / 2)] == (int)ID.WALL)
            {
                playerInfos[playerID].playerID.transform.position = Vector3.Lerp(playerInfos[playerID].playerID.transform.position, originalPosition, 1);
            }
        }

        /// <summary>
        /// 攻撃範囲を回転する時の判定
        /// </summary>
        private void HitBoxTurnJudg(int ID)
        {
            if (beforeInputKey != inputKey)
            {
                switch (beforeInputKey)
                {
                    case "w":
                        switch (inputKey)
                        {
                            case "a":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 270.0f);
                                break;
                            case "s":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 180.0f);
                                break;
                            case "d":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 90.0f);
                                break;
                        }
                        break;
                    case "a":
                        switch (inputKey)
                        {
                            case "w":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 90.0f);
                                break;
                            case "s":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 270.0f);
                                break;
                            case "d":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 180.0f);
                                break;
                        }
                        break;
                    case "s":
                        switch (inputKey)
                        {
                            case "w":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 180.0f);
                                break;
                            case "a":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 90.0f);
                                break;
                            case "d":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 270.0f);
                                break;
                        }
                        break;
                    case "d":
                        switch (inputKey)
                        {
                            case "w":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 270.0f);
                                break;
                            case "a":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 180.0f);
                                break;
                            case "s":
                                playerWeaponHitRange.PlayerRotateHitBox(ID, 90.0f);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            // 攻撃範囲をプレーヤーの進行方向に合わせて動かす為に、一つ前の入力を保存しておく
            if (inputKey != "")
            {
                beforeInputKey = inputKey;
            }
        }

        public void SetBeforeInputKey()
        {
            beforeInputKey = "w";
        }
    }
}