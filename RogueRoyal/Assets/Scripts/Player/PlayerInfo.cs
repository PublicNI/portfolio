﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    /// <summary>
    /// プレイヤーのパラメータ
    /// </summary>
    public class PlayerInfo
    {
        public GameObject playerID;
        public int playerHP = 10;
        public int weaponATK = 2;
        public GameObject[] weaponRange = new GameObject[6];
        public int ranking;
    }
}