﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Dungeon.DungeonInfo;
using static Player.PlayerManager;

namespace Player
{
    public class PlayerGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject player = default;
        [SerializeField] private GameObject playerCamera = default;

        /// <summary>
        /// dungeonMapからPlayerのIDを探して生成
        /// </summary>
        public void Generate()
        {
            int spornPointX;
            int spornPointY;

            // プレイヤーの出現位置が部屋の中ならそこに配置
            while(true)
            {
                spornPointX = Random.Range(0, (width + height) / 2);
                spornPointY = Random.Range(0, (width + height) / 2);

                if (dungeonMap[spornPointX, spornPointY] >= (int)ID.ROOM)
                {
                    break;
                }
            }

            Quaternion rote = Quaternion.Euler(45.0f, 0, 45.0f);
            Instantiate(player, new Vector3(spornPointX - width / 2, 1, spornPointY - height / 2), rote);
        }

        /// <summary>
        /// カメラをプレイヤーに追従させる
        /// </summary>
        public void MovingCamera(int ID)
        {
            // プレーヤーを親に指定
            playerCamera.transform.parent = playerInfos[ID].playerID.transform;

            // 視点をプレーヤーに近づける
            playerCamera.transform.position = new Vector3(playerInfos[ID].playerID.transform.position.x,
                                                          playerInfos[ID].playerID.transform.position.y + 10,
                                                          playerInfos[ID].playerID.transform.position.z - 6);

            playerCamera.transform.rotation = Quaternion.Euler(60.0f, 0, 0);
        }
    }
}